# A first attempt at a reproducible baseline working environment for myself,
# using the Nix package manager.
#
# I'll probably move to home-manager and flakes eventually, but for the moment
# I want a basic working install to, well, start working from.
#
# This has been forked from my personal dotfiles repo.
#
# TODO Figure out how to share package declarations between macOS and NixOS. I
# hear Home Manager can help with that?
#
# Installing unstable packages depends on having the NixOS unstable channel
# added to my channels, as follows:
#
# $ sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable
# $ sudo nix-channel --update
#
# Lifted from here:
#
# https://discourse.nixos.org/t/help-with-installing-unstable-packages/34632/8

{ pkgs, unstable-pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    ## The below may be artifacts of my macOS-specific scripts.

    # Apparently the CA SSL certs get deleted due to the -r flag, rendering
    # nix-env unable to fetch files from HTTPS servers due to SSL cert
    # verification failures. Therefore.
    cacert

    ## End macOS-specific things (I think)

    # Auto-configured .local domain names on the local network are cool.
    avahi

    # Graphical tools for debugging Linux audio hardware and configuration.
    helvum
    pavucontrol

    # It looks ugly, but wev does the job of letting me see what signals
    # are coming from my hardware in a Wayland session. Useful when
    # debugging QMK firmware and/or keyboard layout tweaks.
    wev

    # DVD ripping tools.
    handbrake
    kodi-wayland

    # I have some MacOS < 10 CDs and DVDs that are HFS-formatted.
    hfsprogs

    # Tools for controlling external monitors via DDC/CI.
    ddccontrol

    # Gets lspci, among other things.
    pciutils

    # Sometimes you need to process videos.
    ffmpeg

    # Audacity is a basic but useful sound recorder and editor.
    # Unfortunately, some people got a hold of it and added telemetry.
    # Tenacity is the less-polluted fork.
    tenacity

    # Ardour is a full DAW (digital audio workstation), focused on live
    # recording. When I last tried it on macOS, it was quirky and crashed
    # regularly. Here on NixOS, a quick trial suggests it may actually be
    # usable (though I'd have to learn how to use it).
    ardour

    # I have vague daydreams of someday learning to write decent chiptunes.
    #
    # Commented due to dependency on EOL'd 7.x version of .NET, which was
    # causing my upgrade to NixOS 24.11 to fail.
    #famistudio

    # And, well, given that I'm a professional programmer and amateur
    # synthhead, you might expect that I'd be pretty good with audio
    # programming languages.
    #
    # You'd be wrong, but a man can dream, can't he?
    #
    # ...right now this doesn't actually work. Googling suggests maybe I
    # need to get JACK configured correctly to work with SuperCollider?
    #
    # https://nixos.wiki/wiki/JACK looks kinda sketchy and out-of-date, but
    # fortunately there are rollbacks... :P
    supercollider

    # Sometimes you want to record a screencast or stream video.
    #
    # OBS Studio seems to work just fine for that.
    obs-studio

    # Who doesn't love timezones?
    tzdata

    # If you looked at my Emacs configuration, you'd probably expect me
    # to be a zsh user with a heavily-customized environment, but I stick
    # with bash because I regularly wind up working on systems without my
    # personal configuration - I don't want to get used to idioms I won't
    # have elsewhere.
    #
    # This is here mostly because I want to have a current version of
    # bash on OS X.
    bashInteractive
    bash-completion
    nix-bash-completions

    nodePackages.bash-language-server

    # Let's see if I can get working native compilation in Emacs via Nix.
    emacs28NativeComp

    ## Packages my Emacs configuration depends on.
    #
    # TODO Refactor these out into a shell.nix specific to my Emacs config?
    imagemagickBig

    # Required for packages like emacsql that build binaries if needed.
    clang
    # Useful for C language-server support via LSP.
    clang-tools

    # And some tools (e.g., the libsass PyPi module) require gcc. This is
    # here to support the code for nateeag.com.
    #
    # TODO Put this into a per-project {shell,flake}.nix where it belongs.
    gcc

    ## End Emacs config dependencies.

    # I like ASCII art. Let the cow speak, and the proclamations ring
    # forth.
    cowsay
    figlet

    # I use Anki to help me remember things. Building a current version of
    # Anki from source seems to have defeated the NixOS community thus far,
    # so installing a binary is the thing to do for now.
    anki-bin

    # bat is like cat + less + syntax coloration. It also includes some
    # git integration features that I have no use for.
    #
    # I'm almost embarrassed by the fact that I have it installed, but
    # sometimes it's nice to get syntax coloration when viewing a file
    # you do not want to edit accidentally.
    bat

    # I don't know how I lived before I had direnv. Per-project
    # auto-activated envs are _amazing_.
    direnv

    # Since I currently use Mac OS X as my operating system, I have
    # accumulated scripts that refer to BSD date as 'date' and GNU date
    # as 'gdate'. Therefore, make 'gdate' and friends available to me.
    coreutils-prefixed

    # I have a project or two that depends on fswatch being installed. If
    # I were a purist I would build out nix profiles for those projects,
    # but for the moment this should work.
    fswatch

    # A dependency for building Colossal Cave Adventure. Yes, the right
    # thing is to submit a PR to https://gitlab.com/esr/open-adventure
    # telling it how to do this, but I just wanted to get it working.
    libedit

    # Who doesn't like Zork?
    frotz

    # In my youth I occasionally played FPSes.
    #
    # Now that I'm mid-life I occasionally revert to trying things from my
    # youth to remind myself how young I'm not any more.
    #
    # Xonotic does have a somewhat unique feel, and I've been enjoying
    # getting to know it a bit. Elements of classic UT, some things that
    # seem to be drawn from Quake (which I never played), a few things that
    # seem to be all its own, and the definite flavor of an aged recipe,
    # something that's been refined and tweaked over time by people who've
    # lived with it for years and adjusted it carefully.
    xonotic

    # On rare occasions I've had a great time playing DDR and similar in
    # arcades.
    #
    # I recently discovered StepMania / OutFox exist, and I'm curious.
    stepmania

    # I may yet figure out a way to play my old N64 games.
    mupen64plus
    retroarchFull

    # I am intrigued by the idea of mindustry. Time will tell if I actually
    # like it.
    mindustry

    # I miss Escape Velocity. Endless Sky is a pretty decent reincarnation.
    endless-sky

    # I grew up with Descent, and still have my old Mac OS Classic CDs.
    dxx-rebirth

    # Power Pete was a good time, as a classic MacOS kid.
    #
    # ...this was my very first Nix package.
    unstable-pkgs.mighty-mike

    # Useful for extracting data from classic MacOS HFS-formatted CDs.
    unar

    # Some hero ported all of Pangaea Software's old classic Mac games to Linux
    # and other modern OSes. <3
    nanosaur
    bugdom

    # Delta is a filter for prettifying unified diffs. It makes diffs
    # massively easier to read.
    #
    # By default it hides useful details for simplicity's sake, but it
    # can be configured to leave them intact for those of us used to
    # reading diffs.
    delta

    # Difftastic is a syntax-aware diffing tool based on tree-sitter.
    # It's apparently early days for it yet, but I'm definitely going to
    # give it a shot and see how it does as my git-diff driver.
    difftastic

    # Diffoscope is a general-purpose diff tool that can compare all sorts
    # of different files.
    diffoscope

    # Install Subversion so I have it when I need it.
    subversion

    # Git is my preferred version control system (well, leaving aside the
    # system I slowly plan at https://github.com/NateEag/next-vcs).
    #
    # This is the version that includes git-svn, because I prefer it for
    # dealing with Subversion when feasible.
    gitSVN

    # git-absorb may just be cooler than magit's autosquash
    gitAndTools.git-absorb

    # Another layer on top of git-rebase for managing multiple branches.
    # Perhaps someday I'll take some time to get to know it and see if it's
    # a meaningful improvement. Looks promising.
    git-stack

    # cool-retro-term isn't the most practical of tools, but boy is it
    # pretty.
    cool-retro-term

    # I used to be a heavy GNU screen user in my twenties. I'm giving
    # tmux a try, mainly to see if I like it better than using actual
    # terminal tabs (might be particularly helpful for using terminals in
    # Emacs with libvterm, which I hope to try soon).
    tmux

    # tmate is a fork of tmux that lets you easily share terminal
    # sessions over the internet between desktops (at the price of
    # depending on its website).
    tmate

    # I need this to compile libvterm for use with Emacs, apparently.
    cmake

    # pass is "the standard unix password manager". Your secrets are stored
    # locally as plain text and versioned in a Git repo, making backup and
    # sharing across multiple machines reasonably straightforward. I use it
    # every working day.
    pass

    # Without gpg pass is useless.
    gnupg

    # Without pinentry gpg is not especially helpful.
    pinentry-gnome3
    pinentry-tty

    brasero

    # I sometimes use pass as a source for Git credentials.
    pass-git-helper

    # Backend for a Firefox pass plugin I'm testing out.
    passff-host

    # I'm trying to stick with Firefox as my daily driver, doing my part to
    # prevent the entire web from being under Google's control.
    #
    # Still, if you're gonna write web software, gotta test in Chrome.
    chromium

    # Having CLI clipboard access is generally useful, but this is mostly
    # here so I can use 'pass show -c'.
    wl-clipboard

    # sometimes I need to unzip files.
    unzip

    # Life without clipboard history is really painful.
    #
    # TODO Figure out a reasonable shortcut for this thing.
    #
    # TODO Consider replacing with parcellite? Tried installing it but
    # don't see how to configure it or use it.
    gnomeExtensions.clipboard-history

    # Please let this make jump-to-app shortcuts work...
    gnomeExtensions.run-or-raise

    # Since I'm using Gnome for a desktop environment, let's try
    # dconf-editor for exploring the various schemas available to the
    # gsettings command.
    dconf-editor

    # gnome-tweaks is a GUI app, but it's an easier way to create .desktop
    # files than typing them manually, and I can then add them to my
    # personal dotfiles.
    gnome-tweaks

    # I tend to avoid AWS for my own personal infrastructure, but I
    # regularly work with it, so it's useful to have the official CLI
    # tool installed.
    awscli2

    # MySQL is not my favorite database, but I need to work with it
    # pretty regularly.
    mysql-client

    # A neat-looking tool for basic performance testing of CLI commands.
    # Defaults to multiple runs, is supposed to offer a statistical
    # analysis of results, and will also let you put in multiple commands
    # to get a comparative analysis.
    hyperfine

    # oha seems to be a decent CLI tool for load-testing HTTP servers.
    # Installing it so I can give it a whirl.
    oha

    # wget is a venerable old tool for cloning websites.
    wget

    # Curl is... curl. And I want the dev tools.
    curlFull.dev

    # trurl is an awesome little URL-manipulation Swiss army knife, from
    # the curl team, built with the curl tools.
    trurl

    # xsv is a great tool to have around for wrangling CSVs.
    xsv

    # I use Signal for text chats with several people I care about. It's
    # my preferred medium for its open-source nature and because of the
    # reportedly-robust end-to-end encryption.
    #
    # I care about the encryption not because I'm obsessive about my
    # communications remaining completely secret, but because I do not
    # like nation-states being able to run mass surveillance programs on
    # their citizens easily.
    signal-desktop

    # At a personal threat model level, I'd far rather risk someone
    # reading all my personal messages than risk losing all that history.
    #
    # Hence this tool, which does a good job of letting me archive my
    # messages decrypted. I do it for several reasons:
    #
    # 1) out of worry that I'll lose my encryption keys and lose all that
    # information to eternity, 2) out of a vague concern for future
    # historians, as our all-digital culture is all too likely to lose
    # every ounce of the ephemera that makes up our lives and which
    # historians find so valuable for reconstructing the gory details of
    # the past, and 3) that it is just conceivable my children might want
    # to see what my wife and I had to say to each other in the distant
    # future, and storing our chat logs decrypted at rest means it's not
    # a priori impossible.
    signalbackup-tools

    # Slack is not my favorite communication tool, but it's certainly a
    # pervasive one, and communication tech is kind of bound up in "where
    # are the people you want to talk to?"
    slack

    # Video calls can be nice. meet.jitsi.com is my preferred tool for
    # them, but sometimes people want to use closed-source tools of the
    # surveillance state (I guess because they work better).
    zoom-us

    # It can be handy to run things regularly. I do this mainly for
    # checking email.
    #
    # FIXME Figure out what else I need to do to make cron work.
    cron

    # Tools I use for synchronizing and managing email. Emacs is where I
    # do most of my email from - these are the CLI plumbing to make that
    # possible.
    notmuch
    isync

    # A collection of tools that can help with SSH
    ssh-tools

    # I mostly use Emacs and an Android app for managing my todo.txt
    # files, but just in case...
    todo-txt-cli

    # A todo.txt desktop GUI. It's not flawless, but it's doing the job
    # reasonably well for me.
    unstable-pkgs.sleek-todo

    # Having an environment to learn new languages is nice. When that
    # environment is OSS, it's even better: https://exercism.org
    exercism

    # jp is a tool for querying JSON documents using the JMESPath
    # language. The reasons I try to prefer jp to jq are:
    #
    # * jp is less powerful than jq, which means solutions written using
    #   it will likely be simpler
    #
    # * JMESPath is a standard with a test suite and independent
    #  implementations rather than just a program (and thus tools using
    #  it are at least theoretically vendor-independent)
    #
    # * AWS tools use JMESPath, so if you have its syntax in your head
    #   you'll be better at using AWS tools.
    jp

    # jq is an amazingly overpowered tool for querying JSON documents. It
    # is Turing-complete, as illustrated by this glorious bit of
    # insanity: https://github.com/andrewarchi/wsjq
    #
    # I try to use jp in preference to jq, but sometimes I do find I want
    # the chainsaw.
    jq

    # pup is a CLI HTML query tool. Use CSS selectors to select elements.
    # Answering questions about HTML-bound data is massively easier this
    # way.
    pup

    # xmlstarlet lets you manipulate XML with XPath queries.
    xmlstarlet

    # jid is an interactive tool for building JSON path queries. Isn't
    # strictly compatible with either jq or JMESPath, but is still handy
    # for exploring a fresh dump of JSON from somewhere.
    jid

    # nmap is my preferred tool for exploring networks. I'm not a
    # networking expert - there could well be better options for any
    # number of focused tasks, but it gets the job done for me.
    nmap

    # Send CLI notifications.
    #
    # TODO Install terminal-notifier instead on OS X.
    notify-desktop

    # Ledger is a wonderful CLI tool for managing financial data.
    ledger

    # Explore filesystem hierarchies like it's 1989.
    tree

    # pdfgrep is occasionally handy for extracting text from PDFs (such
    # as PDF copies of financial statements).
    pdfgrep

    # I'd like to try using diffpdf, but as of 2022-06-28 it isn't
    # supported on Darwin. I think it might be useful as a Git diff driver
    # for PDFs, though.
    diffpdf

    # And sometimes you want to fiddle with PDF internals.
    pdftk
    # pdfseparate / pdfunite from Poppler do what they sound like.
    poppler_utils

    # Evince is apparently the official GNOME PDF (and others) viewer.
    evince

    # Image editing tools.
    #
    # Gimp is not the best editor out there, but it sure is comprehensive.
    gimp

    # Being able to sample a color from anywhere on the screen is really
    # useful.
    eyedropper

    # In my teens I enjoyed (and was terrible at) 3D animation.
    #
    # So, what the hey, let's give Blender a whirl.
    blender

    # Ripgrep is a fast tool for searching text files.
    ripgrep

    # fd is for finding files in a directory that meet certain criteria.
    # It's mostly a friendlier, faster alternative to the venerable find
    # command.
    #
    # I'm not sure I'll really use fd much at all, because find is highly
    # portable, but fd does look interesting, so I'm installing it.
    fd

    # Android development packages.
    #
    # FIXME The right way to install these is per-project. That would
    # require figuring out how to use flakes or nix-shell.
    android-tools
    kotlin-language-server

    # CLI for installing Fdroid apps onto a smartphone. I use it to help
    # automate setting up my phone.
    #
    # TODO Move this into a .nix file in my smartphone setup repo.
    fdroidcl

    # plocate is a faster implementation of the standardish Linux tool
    # locate. Commented out because it's only available on Linux and I'm
    # too lazy to figure out how to say "do not install on macOS" atm.
    plocate

    # Sometimes you just need to do some programmatic math in the shell.
    bc

    # Pandoc is a Swiss-army-knife for working with plain-text documents.
    # I have used it for a few jobs in a few different contexts.
    pandoc

    # Aspell isn't a perfect spellchecker, but it works reasonably well
    # for me. Note that I integrate this closely with my Emacs config.
    aspell
    aspellDicts.en

    # I use sdcv to look up words in the dictionary (I have a
    # copyright-free 1913 edition of Webster's for the actual dictionary)
    sdcv

    # I like having a thesaurus around. This is mostly support for my
    # Emacs config, which is where I tend to be when I want a synonym
    # (well, let's face it, Emacs is where I tend to be for the majority
    # of my life :S)
    wordnet

    libreoffice

    # I use several tools written in Haskell and am trying to learn enough
    # to make useful contributions to them.
    #
    # Whether I fall prey to the infamous "can't work in anything else now"
    # effect remains to be seen.
    haskell-language-server
    # FIXME This should really be per-project, as maybe should the language
    # server. For now just trying to get a basic setup working, regardless
    # of Nix ideals and functional purity. :P
    ghc

    ## Nix-related tools / tooling.

    # Language server for the Nix language.
    nil

    # Formatter for the nix programming language.
    nixfmt-rfc-style

    # A tool that can be useful for debugging nix config issues.
    nix-tree

    # A Nix-based tool for provisioning development environments.
    devenv

    # Shellcheck saves me from dumb Bash mistakes on a regular basis. It,
    # too, is here primarily to support my Emacs configuration.
    shellcheck

    # I use syncthing for keeping several files (and collections of
    # files) in sync across multiple devices. Thus, I want it installed
    # automatically.
    syncthing

    # Syncthing is great, but I am a web developer and sometime
    # sysadmin/devoperator, so how can I live without rsync?
    rsync

    # Dog is a DNS lookup tool, essentially dig with a nicer UI. I'll
    # probably always use dig as it's installed almost everywhere by
    # default, but dog looks like it could be nicer for several purposes.
    dogdns

    # A few venerable old DNS exploration tools.
    dig
    whois

    # Sometimes people use xz to compress things. Emacs tarballs, even.
    xz

    # I use a much-beloved ErgoDox EZ keyboard for typing. I run a custom
    # firmware so I can have the layout be what I want, and for that the
    # following tool is handy.
    qmk

    # pipx is a tool for running CLI Python tools in standalone
    # environments. It's here to support bin/install-python-commands.sh in
    # my dotfiles repo.
    pipx

    # I use Python for stuff sometimes.
    #
    # TODO Move my Python projects to using {shell,'or flake'}.nix. For the
    # moment this lets me write 'python -m venv $path' in shell scripts,
    # and that's good enough to limp along with.
    python3

    # Generally this should be specified by individual projects' own
    # requirements, but having these around can be handy for poking at
    # random projects. A lot of different things have Makefiles.
    gnumake
    binutils

    # Get v42l-ctl installed so I can fiddle with my webcam settings.
    #
    # A record of the packages that were not sufficient to make a manual
    # pipx install camset succeed (since my packaging
    # request has gone unfulfilled:
    # https://github.com/NixOS/nixpkgs/issues/205100):
    #
    #     pkg-config
    #     cairo
    #     python310Packages.pygobject3
    #     python310Packages.pycairo
    #     gtk3
    v4l-utils

    # lsusb can be handy.
    usbutils

    # For making bootable USB thumbdrives from ISOs.
    usbimager

    # memtester is a tool for checking if RAM has hardware issues. I'm
    # suspicious mine might.
    memtester

    wine

    # Tools for working with C# / .NET.
    #
    # It's not my favorite language or environment, but it shows up a lot
    # of places.
    dotnet-sdk_8 # Seems this has to be installed manually? :shrug:
    omnisharp-roslyn
    csharprepl

    # It's better to have projects include this themselves and use it in CI
    # for sanity-checking, but good to have it globally for brief
    # contributions to projects that don't have it installed.
    csharpier

    # My dotfiles repo has a node_modules directory containing a number of
    # node CLI tools I depend on. nvm doesn't work on Nix, so to get a
    # global NodeJS I'm just installing it here, instead of per-project.
    # Someday I'll learn to write shell.nix files.
    nodejs

    # Go is a programming language.
    go

    # Gopls is a language server for the Go programming language. By
    # having it installed, I gain decent Go intelligence while editing in
    # Emacs.
    gopls

    # Vagrant is my preferred dev environment creator. I often use
    # it with ansible and virtualbox.
    #
    # The right answer is for each of my dev envs to declare the exact versions
    # they require via flake. Someday.
    vagrant
    ansible
  ];

  _module.args.unstable-pkgs = import <nixos-unstable> {
    config.allowUnfree = true;
  };
}
