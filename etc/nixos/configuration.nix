# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{
  config,
  pkgs,
  ...
}:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    # Include the nixos-hardware projects 12th-gen Framework settings.
    # Assumes I've added the nixos-hardware channel via
    #
    # sudo nix-channel --add https://github.com/NixOS/nixos-hardware/archive/master.tar.gz nixos-hardware
    #
    # TODO Use nixos-hardware without requiring interactive prep. Flake?
    #
    # TODO Get this to explicitly lock to an exact commit. Again, flake?
    <nixos-hardware/framework/13-inch/12th-gen-intel>

    # Include kmonad nixos module manually, because the kmonad package is
    # marked as broken in the nixos package channel (and won't install even
    # if you allow broken packages) as of 2022-11-14.
    #
    # Assumes the kmonad git repo has been previously checked out.
    #
    # TODO Install kmonad without requiring manual git repo checkout.
    # /home/nateeag/third-party/kmonad/nix/nixos-module.nix

    # Include my personal base set of packages.
    ./packages.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  # Configure kmonad, because I customize my laptop keyboard layout. It's not
  # just pointless nerdery - I find the changes help me avoid stress on my wrists.
  #
  # The below config is dependent on the yet-unmerged change to the official
  # kmonad nix module documented in this comment:
  #
  # https://github.com/kmonad/kmonad/issues/648#issuecomment-1321938071
  services.kmonad = {
    enable = true;
    # package = import ./derivations/kmonad.nix { inherit pkgs; };
    keyboards = {
      laptop-internal = {
        defcfg = {
          enable = true;
          fallthrough = true;
          compose.key = null;
        };

        device = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
        config = builtins.readFile /home/nateeag/nixos-framework-setup/kmonad.kbd;
      };
    };
  };

  services.syncthing = {
    enable = true;
    user = "nateeag";
    configDir = "/home/nateeag/.config/syncthing";
  };

  # Try out fingerprint reader support... really not sure if I want to use it
  # or not.
  services.fprintd.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # A pair of hacks that are supposed to prevent the 12th gen Intel CPU / GPU
  # desktop freeze:
  #
  # https://discourse.nixos.org/t/intel-12th-gen-igpu-freezes/21768/5
  #
  # FIXME Revert this change if I have another flickering-screen freeze. That
  # experience would indicate that the reported freeze isn't the cause of the
  # issues I'm experiencing.
  services.xserver.videoDrivers = [ "modesetting" ];
  boot.kernelParams = [ "i915.enable_psr=1" ];

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Use Gnome and GDM for my default desktop session
  services.displayManager.defaultSession = "gnome";

  # Set my various gnome gsettings-based customizations.
  #
  # N.B.: this apparently only takes effect if you haven't made changes via
  # other means, if I understand https://github.com/NixOS/nixpkgs/issues/66554
  # correctly.
  #
  # The tap-and-drag settings make it way easier to drag things over a long
  # distance (as long as you move quickly - is there some way to shut off or
  # customize that timeout)?
  #
  # xkb-options is set to empty array to prevent it from containing the item
  # 'terminate:ctrl_alt_bksp', which I often type inadvertently (it deletes
  # backwards one word on macOS with Emacs keybindings enabled), as well as the
  # ralt:v3 (if I remember the name right) setting that makes my right alt key
  # not recognized as Meta in Emacs.
  #
  # FIXME Figure out how to make the above changes take effect. Rebuilding
  # NixOS seems to restore the default settings that I don't want, if I'm
  # understanding the results I'm getting.
  services.xserver.desktopManager.gnome.extraGSettingsOverrides = ''
    [org.gnome.desktop.interface]
    gtk-key-theme='Emacs'

    [org.gnome.desktop.peripherals.touchpad]
    natural-scroll=false
    tap-to-click=true
    tap-and-drag=true
    tap-and-drag-lock=true

    [org.gnome.settings-daemon.plugins.color]
    night-light-enabled=true

    [org.gnome.desktop.input-sources]
    xkb-options=[]

    [org.gnome.shell.extensions]
    enabled-extensions=['clipboard-history@alexsaveau.dev','run-or-raise@edvard.cz']
  '';

  # Configure keymap in X11
  services.xserver = {
    xkb = {
      layout = "us";
      variant = "";
    };
  };

  # I like automatic .local domain names.
  #
  # They started working at some point, and it's not clear to me whether the
  # following configuration had anything to do with it.
  services.avahi = {
    enable = true;
    openFirewall = true;
    nssmdns4 = true;
    publish = {
      enable = true;
      addresses = true;
      workstation = true;
    };
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.nateeag = {
    isNormalUser = true;
    description = "Nate Eagleson";
    # The "input" and "uinput" groups were added for kmonad.
    #
    # TODO Figure out if the kmonad nixos module actually requires me to
    # manually add groups for my user.
    extraGroups = [
      "networkmanager"
      "wheel"
      "input"
      "uinput"
    ];
    packages = with pkgs; [
      (firefox.override { nativeMessagingHosts = [ passff-host ]; })
      firefox
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
  ];

  # I occasionally want to play video games. Sometimes I get them via Steam.
  # This is an attempt to get it working on NixOS, per the instructions
  # at https://nixos.wiki/wiki/Steam
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Reclaim disk space from old setups automatically, without making it _too_
  # hard to roll back to a past version I know worked...
  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 30d";

  xdg.mime.defaultApplications = {
    "application/pdf" = "org.gnome.evince.desktop";
  };

  # Yay for turning on experimental features I don't yet fully understand! :P
  #
  # I know I want to start moving towards flakes eventually, and I keep trying
  # to run commands from the net and hitting "experimental feature nix-command
  # is disabled".
  #
  # So.
  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
