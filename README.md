% NixOS Framework Setup

I've been an Apple desktop user since I was four, blindly typing ProDOS
commands I didn't understand.

As a result, I've been using something Unixy since Mac OS X was first
released.

NixOS caught my attention years ago by being the package manager / OS I'd
daydreamed of for years, except being far smarter about it than I'd
managed to work out.

I'm finally taking a stab at using it for my personal desktop environment,
after several months of managing my OS X CLI tools using nix (very naively).

## Useful Notes

Things I've had to figure out and don't want to forget (Anki these, maybe?):

- journalctl is the nixos equivalent to /var/syslog - the collection of system
  events. Good starting point for debugging weirdness.


## Making NixOS Packages

This wiki page has the basic "here's how to hack on a package" loop that I
thought I'd have to work out myself:

https://nixos.wiki/wiki/Packaging/Tutorial

I'll definitely need to expand my knowledge from more-canonical resources, but
it looks like the core of what I've been missing.

This seems to be a more-recent doc that covers some similar ground.

https://nixos.wiki/wiki/Nixpkgs/Create_and_debug_packages

## Firefox Config

I've used Chrome for years, but NixOS comes with FF and it's working okay for
me. Maybe I'll swap back.

https://github.com/NixOS/nixpkgs/blob/master/doc/builders/packages/firefox.section.md
has an overview of how I might be able to declaratively configure my
extensions.

So far, my extensions are:

- vimium
- PassFF (with to-be-changed Ctrl+Shift+U keybinding and "Behavior of Enter
  Key" set to "Fill and submit"), which I really ought to change over to
  [Browserpass](https://github.com/browserpass/browserpass-extension), the
  extension I'm using on my work MBP

I should also add:

- ActivityWatch Web Watcher

- Stylus (for theming websites as I have need)


## Personal Hotkey Namespace

For many years I've used Ctrl+Shift+<key> as my personal global keyboard
shortcuts on macOS.

On a Gnome desktop, that's not a feasible global combo, as many Linux programs
use Ctrl+Shift as a shortcut combo.

So what do I want?

Probably double-tapping + hold to send Hyper (or something which can be
interpreted as such on OSes)

On my MBP it'd be double-tap-and-hold Command, right next to Spacebar.

On my ErgoDox EZ, I think I'd want to send it with double-tap-and-hold of Super
(a.k.a. Command).

On my Framework, I'd want it to be sent by double-tap-and-hold Alt, as that's
the key on either side of Spacebar. Super isn't symmetrical on that keyboard.

So what's the best way to have global keyboard shortcuts in Linux?

I have installed the `gnomeExtensions.run-or-raise` extension, which seems to
work fine for focusing/starting apps. I can presumably use the Gnome Settings >
Keyboard > Keyboard Shortcuts dialog to set up running arbitrary command-line
scripts with keyboard shortcuts.

However, making sure those don't overlap with any existing Linux app shortcuts
is really hard, because they're so inconsistent.

The best bet is to use Hyper as the modifier, as it's virtually unused due to
its absence on modern keyboards.

...alas, it's so rarely-used that the USB keyboard standards don't _have_ it,
and thus low-level remapping tools like kmonad and friends cannot send Hyper
keys: https://github.com/kmonad/kmonad/issues/22

Thus, you have to listen for some _other_ keypress (perhaps, say, F16, rarely
used on any physical keyboard and certainly not on any I own) and translate
_that_ to Hyper using the appropriate tools for your DE.

Under X, people seem to use xmodmap or xkb.

Poking around the Interwebs, xkb is still apparently the answer for Gnome under
Wayland (or possibly Gnome-tweaks which has some options for this).

https://gist.github.com/toroidal-code/ec075dd05a23a8fb8af0 may be helpful.

https://stackoverflow.com/a/72993460 suggests a simple way to map caps lock to
Hyper. Maybe I could use that for an initial test, and perhaps adapt it for
translating a high-numbered Fkey to Hyper?

https://nixos.org/manual/nixos/stable/#custom-xkb-layouts seems to cover
everything I'd need to put this in my NixOS config once I've figured out how to
define a Hyper key (presumably using F19 or similar, then teaching my various
physical devices to send F19 when desired).

I've added a custom shortcut to Gnome of Super-Control-Shift-N that runs
`notify 'Yo yo yo'`, and confirmed that actually does trigger a notification.

The next step would be to test triggering that with a Hyper key for the
shortcut.




## Auto-position Gnome App Windows

On macOS I use Hammerspoon to automatically put windows where I want, triggered
by both a keybinding and by "display added" / "display removed" events.

How can I do that with a Gnome desktop?

Looks like the smart-auto-move extension might be worth a try?

https://search.nixos.org/packages?channel=22.11&show=gnomeExtensions.smart-auto-move&from=0&size=50&sort=relevance&type=packages&query=smart-auto-move

Apparently in Gnome, Super+{Left,Right} toggles between full-screen,
half-screen, and standard window size.

Super+Shift+{Left,Right,Up,Down} moves windows between displays.

Those are clumsy-but-workable, and cover most of the window manipulation I
usually do (I will on rare occasions make a window fill a quarter of the
screen).

If I can figure out a way to bind arbitrary keystrokes for those operations,
I'd be getting somewhere.


## Set Up App Shortcuts With run-or-raise

The extension seems to do what I want, but it'll be useless until I actually
get the shortcuts configured in a way that works for me.


## Bump nixos-hardware Framework Module To Get Better Battery Life?

Looks like the current version of the module has better battery life. So I
should probably update it.


## Need To Install camset for color-balancing webcam

https://github.com/azeam/camset

Alas, I didn't find a NixOS package on first try. Maybe I should look more
closely?

I've [filed a packaging
request](https://github.com/NixOS/nixpkgs/issues/205100) that has gone
unfulfilled for quite some time. Maybe that can be my first Nix package.


## Figure Out Why USB Headset Sometimes Won't Appear In Settings

It usually does.

On 2022-12-07 it wouldn't. I don't know why.

I installed the usbutils package and took a look to try to figure out why, but
came up empty.


## Upgrading NixOS Release Version

Nix calls these "channels" - basically, making sure I have the most recent
stable set of packages available / installed.

I've got a script misleadingly-named `upgrade-nixos-version` where I'm evolving
some automation for this.

It's common for configuration options and packages to be renamed across NixOS
versions. When that happens, `upgrade-nixos-version` will fail. To resolve it,
update the config accordingly and run the last rebuild step manually until you
reach success.

## Upgrading Signal

Signal Desktop occasionally expires, which means I can't message with it any
more.

To fix that, I need to update the package.

I just tried

```bash
sudo nix-channel --update
sudo rebuild-nixos-conf
```

which did work.

but it took forever, because it updates every single package that's changed in
the channel. Worth doing occasionally, but annoyingly slow.

How can I upgrade just Signal Desktop?

This thread seems relevant:
https://www.reddit.com/r/NixOS/comments/10vlo0t/possible_to_upgrade_individual_packages_without/


## Dock / HDMI Woes Debugging

I have a new laptop dock on which the USB-C display 1 sometimes works with my
NixOS Framework and sometimes does not. It always works with my work MBP, so I
suspect the problem is either the Framework's hardware or NixOS configuration.

I've been able to resolve the problem once by shutting down the laptop,
powering it up, then plugging in the dock.

Display port 2 (via a different USB-C cable) does not do power delivery, and
works reliably.

Usually I can disconnect the HDMI cable connecting the dock's port 1 to the
external monitor, and connect it directly to an HDMI port on the Framework. I
saw that fail on 2023-04-26.

I've come to the tentative conclusion this is due to hardware issues. I haven't
got great evidence of that but it seems to fit the data I've seen so far.


## kmonad Setup

There's a very basic kmonad configuration replicating the layout I have in my
dotfiles for use with Karabiner-Elements on OS X. Since kmonad is available on
OS X, I may eventually try porting this layout to my dotfiles so I can use the
same program on both OSes.


## N64 Emulation

I'm trying to get my N64 games playable on here.

RetroArch is an incredibly-confusing UI.

That said, it seems to have the right settings for Mario64 to at least load and
run reasonably at the start, which the CLI version of mupen64plus doesn't seem
to do.

I was able to get it to run by:

* starting RetroArch
* loading the N64 Mupen64plus core
* manually navigating to where my Mario64 ROM lives (yes, I do own the cart,
  and yes, I have ripped it myself :P)

I have not figured out how to get my 8bitdo controller to work yet, though. It
connects to Bluetooth, but RetroArch doesn't see it.

...ah. Apparently I had to use it with the switch set to D. And maybe add that
udev rule config but not sure if that's necessary.

So I just played Super Hexagon with it and that worked.

RetroArch sees the controller now, and I can drive the RetroArch UI, but it
doesn't seem to be able to hook it up when I actually fire up Mario64. :/

The problem seems to be that RetroArch is convinced I have an Xbox controller
set up when I am not aware of having one.

mupen64plus on the CLI seems to think the same thing.

I don't know why.


## Old Mac Games

### Bungie

I was, for decades, a big fan of Bungie Studios' games.

I haven't really bothered with anything post-Halo-3 (which for me was a letdown).

That said, I'm looking for ways to ensure the classic Bungie games remain
playable for the foreseeable future.

Already available as NixOS packages:

- The Marathon trilogy is already well-preserved in the form of Aleph One.

- Pathways Into Darkness has a decent-looking AlephOne port: https://simplici7y.com/items/aleph-one-pathways-into-

Not yet available as NixOS packages:

- Myth / Myth II. I've successfully run Myth II via steam-run, so a real
  package shouldn't be hard to put together.

- Oni. https://wiki.oni2.net/OniX seems to be an executable that would likely
  work via steam-run thanks to Proton. Alas, I only have the PS2 and Mac CDs,
  and while the Mac data should work with OniX, I have yet to find a way to
  read the data on my NixOS machine. It seems likely the CD is vanilla HFS, and
  [hfsutils](https://www.mars.org/home/rob/proj/hfs/) doesn't seem to have been
  packaged for NixOS yet. Guess it's time to try making a package.

  My draft hfsutils package let me extract the data from my dics, but now I'll
  need to jump through the hoops to get OniX actually working with that data.

### Descent

I played a lot of Descent and Descent II in my youth. I still have the discs.

dxx-rebirth should be capable of using that data:

https://github.com/dxx-rebirth/dxx-rebirth/blob/bd3c033bdf1faa4606086dcae0436531fb2e7e5c/d2x-rebirth/INSTALL.txt#L98

and as the docs suggest, The Unarchiver should be able to extract the data
files from the Mac OS Classic installers. I'm not running a Mac, but NixOS has
a package for [unar](), the cross-platform, source-available CLI version of The
Unarchiver.

Tested with Descent II, and it appears to have worked. Can I fire up the game?

...hey, Descent works. Albeit without any sound effects, apparently.

And Descent II has working SFX. Wonder what I missed for Descent?
