#! /usr/bin/env bash

# A script for extracting the Oni game data files off the original MacOS HFS
# CD-ROM.
#
# Depends on hfsutils being installed and available.
#
# The right way to do that would be to get a working package build installing
# the binaries.
#
# I'm currently just adding the build directory to my path before executing
# this, because I haven't gotten my package all the way to a working state - it
# builds but won't install.
#
# _Really_ doesn't belong in this repo, since it's completely specific to
# archiving MacOS Oni game data.

script_name="$(basename "$0")"
usage_msg="Usage: $script_name </dev/cd_rom> <data/dump/directory>"

if [ $# -ne 2 ]; then
    echo "$usage_msg" >&2

    exit 1
fi

target_dir="$2"

if [ ! -d "$target_dir" ]; then
    echo "$usage_msg" >&2

    echo "'$target_dir' is not a directory!" >&2

    exit 1
fi

# TODO: Write something to recursively copy HFS directory structures? I have
# other old discs I would like to extract the data from.
#
# Maybe hfsutils has that built-in somewhere and I just haven't found it?
hmount "$1"
mkdir -p "$target_dir/IGMD"
# hcopy -m ':Oni\ ?:GameDataFolder:IGMD:*' "$target_dir/"

level_dirs='Airport Airport_III compound dream_lab EnvWarehouse global lab manplant neuro power power_II roof state tctf tctf_ii'

for level in $level_dirs; do
    level_dir="$target_dir"/GameDataFolder/IGMD/"$level"
    mkdir -p "$level_dir"

    hfs_level_dir=":Oni\ ?:GameDataFolder:IGMD:$level"
    hcopy -m "$hfs_level_dir"':*' "$level_dir"
done

humount
